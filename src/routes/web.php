<?php

Route::group(['namespace'=>'Bryanjack\Git\Controllers'], function(){
	Route::post('/git', 'GitController@parse');
	Route::get('testchat', 'GitController@chat');
	Route::get('/date', 'GitController@date');
});
